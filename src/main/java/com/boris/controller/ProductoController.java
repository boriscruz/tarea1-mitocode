package com.boris.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.boris.model.Producto;
import com.boris.service.ProductoService;

@RestController
@RequestMapping(value = "/productos")
public class ProductoController {

	@Autowired
	private ProductoService service;

	@GetMapping(value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Producto>> toList() {
		List<Producto> productos = new ArrayList<>();
		productos = service.listar();
		return new ResponseEntity<List<Producto>>(productos, HttpStatus.OK);
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Producto> toListId(@PathVariable("id") Integer id) {
		Producto producto = new Producto();
		producto = service.buscarPorId(id);
		return new ResponseEntity<Producto>(producto, HttpStatus.OK);
	}

	@PostMapping(value = "/agregar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Producto> toRegister(@RequestBody Producto producto) {
		Producto pro = new Producto();
		try {
			pro = service.agregar(producto);
		} catch (Exception e) {
			return new ResponseEntity<Producto>(pro, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Producto>(pro, HttpStatus.OK);
	}

	@PutMapping(value = "/modificar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> update(@PathVariable Producto producto) {
		int result = 0;
		try {
			service.modificar(producto);
			result = 1;
		} catch (Exception e) {
			result = 0;
		}
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}

	@DeleteMapping(value = "/eliminar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> delete(@PathVariable Integer id) {
		int result = 0;
		try {
			service.eliminar(id);
			result = 1;
		} catch (Exception e) {
			result = 0;
		}
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}

}
