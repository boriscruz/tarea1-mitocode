package com.boris.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.boris.model.DetalleVenta;
import com.boris.service.DetalleVentaService;

@RestController
@RequestMapping(value = "/detalle-ventas")
public class DetalleVentaController {

	@Autowired
	private DetalleVentaService service;

	@GetMapping(value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<DetalleVenta>> toList() {
		List<DetalleVenta> detalleVentas = new ArrayList<>();
		detalleVentas = service.listar();
		return new ResponseEntity<List<DetalleVenta>>(detalleVentas, HttpStatus.OK);
	}

	@PostMapping(value = "/agregar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<DetalleVenta> toRegister(@RequestBody DetalleVenta detalleVentas) {
		DetalleVenta vent = new DetalleVenta();
		try {
			vent = service.agregar(detalleVentas);
		} catch (Exception e) {
			return new ResponseEntity<DetalleVenta>(vent, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<DetalleVenta>(vent, HttpStatus.OK);
	}

}
