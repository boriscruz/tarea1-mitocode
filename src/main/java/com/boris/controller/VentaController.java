package com.boris.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.boris.model.Venta;
import com.boris.service.VentaService;

@RestController
@RequestMapping(value = "/ventas")
public class VentaController {

	@Autowired
	private VentaService service;

	@GetMapping(value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Venta>> toList() {
		List<Venta> ventas = new ArrayList<>();
		ventas = service.listar();
		return new ResponseEntity<List<Venta>>(ventas, HttpStatus.OK);
	}

	@PostMapping(value = "/agregar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Venta> toRegister(@RequestBody Venta venta) {
		Venta vent = new Venta();
		try {
			vent = service.agregar(venta);
		} catch (Exception e) {
			return new ResponseEntity<Venta>(vent, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Venta>(vent, HttpStatus.OK);
	}

}
