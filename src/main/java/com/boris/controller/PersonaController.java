package com.boris.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.boris.model.Persona;
import com.boris.service.PersonaService;

@RestController
@RequestMapping(value = "/personas")
public class PersonaController {
	
	@Autowired
	private PersonaService service;
	
	@GetMapping(value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Persona>> toList() {
		List<Persona> personas = new ArrayList<>();
		personas = service.listar();
		return new ResponseEntity<List<Persona>>(personas, HttpStatus.OK);
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Persona> toListId(@PathVariable("id") Integer id) {
		Persona persona = new Persona();
		persona = service.buscarPorId(id);
		return new ResponseEntity<Persona>(persona, HttpStatus.OK);
	}

	@PostMapping(value = "/agregar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Persona> toRegister(@RequestBody Persona persona) {
		Persona per = new Persona();
		try {
			per = service.agregar(persona);
		} catch (Exception e) {
			return new ResponseEntity<Persona>(per, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<Persona>(per, HttpStatus.OK);
	}

	@PutMapping(value = "/modificar", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> update(@PathVariable Persona persona) {
		int result = 0;
		try {
			service.modificar(persona);
			result = 1;
		} catch (Exception e) {
			result = 0;
		}
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}

	@DeleteMapping(value = "/eliminar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> delete(@PathVariable Integer id) {
		int result = 0;
		try {
			service.eliminar(id);
			result = 1;
		} catch (Exception e) {
			result = 0;
		}
		return new ResponseEntity<Integer>(result, HttpStatus.OK);
	}

}
