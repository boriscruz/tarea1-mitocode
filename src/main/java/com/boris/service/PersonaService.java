package com.boris.service;

import java.util.List;

import com.boris.model.Persona;

public interface PersonaService {
	
	Persona agregar(Persona persona);
	
	void modificar(Persona persona);
	
	void eliminar(Integer idPersona);
	
	Persona buscarPorId(Integer idPersona);
	
	List<Persona> listar();

}
