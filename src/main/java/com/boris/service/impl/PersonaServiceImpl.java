package com.boris.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.boris.dao.PersonaDAO;
import com.boris.model.Persona;
import com.boris.service.PersonaService;

@Service
public class PersonaServiceImpl implements PersonaService {

	@Autowired
	private PersonaDAO dao;

	@Override
	public Persona agregar(Persona persona) {
		return dao.save(persona);
	}

	@Override
	public void modificar(Persona persona) {
		dao.save(persona);
	}

	@Override
	public void eliminar(Integer idPersona) {
		dao.deleteById(idPersona);
	}

	@Override
	public Persona buscarPorId(Integer idPersona) {
		return dao.getOne(idPersona);
	}

	@Override
	public List<Persona> listar() {
		return dao.findAll();
	}

}
