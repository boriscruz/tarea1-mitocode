package com.boris.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.boris.dao.DetalleVentaDAO;
import com.boris.model.DetalleVenta;
import com.boris.service.DetalleVentaService;

@Service
public class DetalleVentaServiceImpl implements DetalleVentaService {

	@Autowired
	private DetalleVentaDAO dao;

	@Override
	public DetalleVenta agregar(DetalleVenta detalleVenta) {
		return dao.save(detalleVenta);
	}

	@Override
	public List<DetalleVenta> listar() {
		return dao.findAll();
	}

}
