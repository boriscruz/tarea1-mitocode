package com.boris.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.boris.dao.VentaDAO;
import com.boris.model.Venta;
import com.boris.service.VentaService;

@Service
public class VentaServiceImpl implements VentaService {

	@Autowired
	private VentaDAO dao;

	@Override
	public Venta agregar(Venta venta) {
		venta.getDetalleVenta().forEach(x -> x.setIdVenta(venta));
		return dao.save(venta);
	}

	@Override
	public List<Venta> listar() {
		return dao.findAll();
	}

}
