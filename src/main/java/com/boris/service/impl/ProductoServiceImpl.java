package com.boris.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.boris.dao.ProductoDAO;
import com.boris.model.Producto;
import com.boris.service.ProductoService;

@Service
public class ProductoServiceImpl implements ProductoService {

	@Autowired
	private ProductoDAO dao;

	@Override
	public Producto agregar(Producto producto) {
		return dao.save(producto);
	}

	@Override
	public void modificar(Producto producto) {
		dao.save(producto);
	}

	@Override
	public void eliminar(Integer idproducto) {
		dao.deleteById(idproducto);
	}

	@Override
	public Producto buscarPorId(Integer idproducto) {
		return dao.getOne(idproducto);
	}

	@Override
	public List<Producto> listar() {
		return dao.findAll();
	}

}
