package com.boris.service;

import java.util.List;

import com.boris.model.DetalleVenta;

public interface DetalleVentaService {

	DetalleVenta agregar(DetalleVenta detalleVenta);

	List<DetalleVenta> listar();

}
