package com.boris.service;

import java.util.List;

import com.boris.model.Venta;

public interface VentaService {

	Venta agregar(Venta venta);

	List<Venta> listar();

}
