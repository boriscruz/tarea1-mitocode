package com.boris.service;

import java.util.List;

import com.boris.model.Producto;

public interface ProductoService {

	Producto agregar(Producto producto);

	void modificar(Producto producto);

	void eliminar(Integer idproducto);

	Producto buscarPorId(Integer idproducto);

	List<Producto> listar();

}
